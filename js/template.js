$ = jQuery;

$(window).load(function(){

    var theWindow        = $(window),
        $bg              = $("#bg"),
        aspectRatio      = $bg.width() / $bg.height();
                                
    function resizeBg() {       
        if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
            $bg
                .removeClass()
                .addClass('bgheight');
        } else {
            $bg
                .removeClass()
                .addClass('bgwidth');
        }           
    }      

    $("#preloader").delay(1000).fadeOut("fast");

    theWindow.resize(resizeBg).trigger("resize");

});

$(document).ready(function(){

	window.dispatchEvent(new Event("resize"));

	scrollTimeout = null;
	$(".bottom-controls").css("top",  $(window).height() + $(document).scrollTop());

	//*

	$(document).on("scroll", function(){

		$(".bottom-controls").css("top", 9999);
		$(".bottom-controls").css("opacity", "0");

		if (scrollTimeout !== null) {
			clearTimeout(scrollTimeout);
			scrollTimeout = null;
		}

		if (scrollTimeout == null) {
			
			scrollTimeout = setTimeout(function(){
				var height = $(window).height();
				var scrollTop = $(document).scrollTop();

				$(".bottom-controls").css("opacity", "1");
				$(".bottom-controls").css("top", height + scrollTop + 44);
				$(".bottom-controls").stop().animate({top: height + scrollTop - 44});

				scrollTimeout = null;

				//console.log("Scroll debounce ended!");

			}, 100);
		}

		//console.log("Scrolling...");

	});//*/


	/*
    $('.footer-backtotop, .deploy-navigation').click(function() {
		$('body,html').animate({
			scrollTop:0
		}, 200, 'easeOutExpo');
		return false;
	});*/

	$('.deploy-navigation').click(function(){
		$('.page-navigation').addClass('page-navigation-active');
		$('.close-navigation').show(0);
		$(this).hide(0);

		window.scrollTo(0,0);

		return false;
	});
		
	$('.close-navigation').click(function(){
		$('.page-navigation').removeClass('page-navigation-active');
		$('.deploy-navigation').show(0);
		$(this).hide(0);
		return false;
	});

	$('.nav-item').click(function(){
		$('.close-navigation').click();
	});
	
	$('.page-navigation a').hover(function(){
		$(this).find('i:first-child').toggleClass('hover-icon');		
	});
	
	$('.submenu-deploy').click(function(){
		$(this).parent().find('.submenu-items').toggleClass('submenu-items-active');
		$(this).find('.fa-angle-down').toggleClass('deploy-navigation-active');
		return false;
	});
	
	//Submenu Nav
	
	$('.submenu-nav-deploy').click(function(){
		$(this).toggleClass('submenu-nav-deploy-active');
		$(this).parent().find('.submenu-nav-items').toggle(100);
		return false;
	});
	
	/* Detect if iOS WebApp Engaged and permit navigation without deploying Safari */

	(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(d.href.indexOf("http")||~d.href.indexOf(e.host))&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone")
});